--codemd: begin segment=Float caption=Instantiation for Float
with critical;

package critical_float is new critical (Float, "=");
--codemd: end
