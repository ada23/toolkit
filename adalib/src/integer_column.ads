with tables;

package Integer_Column is new tables.ColumnPkg
  (Integer, Integer'Value, Integer'Image);
