package numlib.differentiation is

   function Derivative (f : FxPtr; 
                        X1, X2 : RealType ) return RealType;

end numlib.differentiation ;
