#!/bin/bash

pushd button; alr build; popd
pushd clserv; alr build; popd
pushd csvtest; alr build; popd
pushd cvar; alr build; popd
pushd dglogs; alr build; popd
pushd diffint; alr build; popd
pushd dump; alr build; popd
pushd frep; alr build; popd
pushd gitrev; alr build; popd
pushd integ; alr build; popd
# pushd interf; alr build; popd
pushd logs; alr build ; popd
pushd meals; alr build; popd
pushd morse; alr build; popd
pushd num; alr build; popd
#pushd pid; alr build; popd
pushd print; alr build ; popd
pushd ptable; alr build ; popd
pushd revarg; alr build; popd
pushd splitter; alr build; popd
pushd stats; alr build; popd
pushd varname; alr build; popd
pushd vrex; alr build; popd