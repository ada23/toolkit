--------------------------------------------
-- Created 2025-01-20 11:14:35
--------------------------------------------
package revisions is
	dir : constant String := "/Users/rajasrinivasan/Prj/GitLab/toolkit/examples/gitrev" ;
	version : constant String := "0.0.1" ;
	repo : constant String := "git@gitlab.com:ada23/toolkit.git" ;
	commitid : constant String := "b389ae938dcf633538c87062b51fe8e2b2ebcbda" ;
	abbrev_commitid : constant String := "b389ae9" ;
	branch : constant String := "main" ;
end revisions ;
