--------------------------------------------
-- Created 2025-01-20 10:40:22
--------------------------------------------
package revisions is
	dir : constant String := "/Users/rajasrinivasan/Prj/GitLab/toolkit/examples/gitrev" ;
	version : constant String := "0.0.2" ;
	repo : constant String := "git@gitlab.com:ada23/toolkit.git" ;
	commitid : constant String := "bbc1d79bdac1eacd0c3fd7cac22dd01022eff956" ;
	abbrev_commitid : constant String := "bbc1d79" ;
	branch : constant String := "main" ;
end revisions ;
